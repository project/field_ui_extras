<?php

namespace Drupal\field_ui_extras\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a FieldSummary annotation object.
 *
 * Note that the "@ Annotation" line below is required and should be the last
 * line in this docblock. It make this annotation definition discoverable.
 *
 * @see \Drupal\field_ui_extras\FieldSummaryPluginManager
 * @see plugin_api
 *
 * @Annotation
 */
class FieldSummary extends Plugin {

  /**
   * Type of field (e.g., text, date, datetime, file, image, decimal, etc.).
   *
   * @var string
   */
  public $fieldType;

}
