<?php

namespace Drupal\field_ui_extras\Form;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Field\FieldTypePluginManager;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field_ui_extras\FieldSummaryPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /** @var string Config settings */
  const SETTINGS = 'field_ui_extras.settings';

  /**
   * Field Type Manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManager
   */
  protected $fieldTypeManager;

  /**
   * Field UI Extras Field Summary Plugin Manager.
   *
   * @var \Drupal\Core\Field\FieldTypePluginManager
   */
  protected $fieldSummaryManager;

  /**
   * Array of field types (machine names).
   *
   * @var array
   */
  private $fieldTypes = [];

  /**
   * Constructor to handle dependencies.
   *
   * @param \Drupal\Core\Field\FieldTypePluginManager $field_type_manager
   *   The field type plugin manager.
   * @param \Drupal\field_ui_extras\FieldSummaryPluginManager $field_summary_manager
   *   The Field UI Extras field summary manager.
   */
  public function __construct(FieldTypePluginManager $field_type_manager, FieldSummaryPluginManager $field_summary_manager) {
    $this->fieldTypeManager = $field_type_manager;
    foreach ($this->fieldTypeManager->getDefinitions() as $key => $val) {
      $this->fieldTypes[$key] = [];
    }

    $this->fieldSummaryManager = $field_summary_manager;
    $field_summary_definitions = $this->fieldSummaryManager->getDefinitions();
    foreach ($field_summary_definitions as $def) {
      if (isset($this->fieldTypes[$def['fieldType']])) {
        $this->fieldTypes[$def['fieldType']][$def['id']] = $def['id'];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.field.field_type'),
      $container->get('plugin.manager.field_ui_extras.field_summary')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'field_ui_extras_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $intro = '<h2>'
    . $this->t('Choose a Field Summary Plugin for Each Field Type')
    . '</h2><em>'
    . $this->t('All available FieldSummary plugins will show as options below.')
    . '</em>';

    $form['a_intro'] = [
      '#markup' => $intro,
    ];

    if (!empty($this->fieldTypes)) {
      $this->addFieldTypeFieldsToForm($form);
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * Adds field type fields to the form.
   *
   * @param array $form
   *   Drupal form.
   */
  public function addFieldTypeFieldsToForm(array &$form): void {
    $config = $this->config(static::SETTINGS);

    $field_types = array_keys($this->fieldTypes);
    sort($field_types);
    $weight_for_each_field_type = array_flip($field_types);

    foreach ($this->fieldTypes as $ftype => $summary_plugins) {
      if (empty($summary_plugins)) {
        continue;
      }

      $default = '';
      $current_mapping_from_config =
        $config->get('field_type_summary_plugin_mappings.' . $ftype);
      if ($summary_plugins[$current_mapping_from_config]) {
        $default = $summary_plugins[$current_mapping_from_config];
      }

      $options = $summary_plugins;
      $form['ftype_' . $ftype] = [
        '#title' => '<h3>' . $ftype . '</h3>',
        '#type' => 'radios',
        '#options' => $options,
        '#default_value' => $default,
        '#weight' => $weight_for_each_field_type[$ftype] + 100,
      ];

      $form['actions']['#weight'] = 1000;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $mappings = [];
    foreach ($form_state->getValues() as $key => $value) {
      if (strpos($key, 'ftype_') === 0) {
        $mappings[substr($key, 6)] = $value;
      }
    }

    $config = $this->configFactory->getEditable(static::SETTINGS);
    $config->set('field_type_summary_plugin_mappings', $mappings);
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
