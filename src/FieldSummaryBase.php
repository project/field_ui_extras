<?php

namespace Drupal\field_ui_extras;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Plugin\PluginBase;

/**
 * Defines a base class to help developers implement their own plugins.
 *
 * This handles tasks that are common to most FieldSummary plugins, which
 * should reduce the amount of code needed to create a new plugin of this type.
 *
 * We can read properties from the @FieldSummary annotation.
 *
 * @see \Drupal\field_ui_extras\Annotation\FieldSummary
 * @see \Drupal\field_ui_extras\FieldSummaryInterface
 */
abstract class FieldSummaryBase extends PluginBase implements FieldSummaryInterface {

  /**
   * Current field's Field Config. $config.
   *
   * @var \Drupal\field\FieldConfigInterface
   */
  public $fConfig;

  /**
   * Current field's Storage Config. $config->fieldStorage.
   *
   * @var \Drupal\field\entity\FieldStorageConfig
   */
  public $fStorageConf;

  /**
   * Array of current field's storage settings. $config->fieldStorage->settings.
   *
   * @var array
   */
  public $fStorageSettings;

  /**
   * Array of current field's widget settings. $config->settings.
   *
   * @var array
   */
  public $fWidgetConf;

  /**
   * Array of current field's info items to include in the output.
   *
   * Use this to build an array of property => value, property => value
   * which will be rendered, by default, as an unordered list.
   *
   * @var array
   */
  public $items;

  /**
   * {@inheritdoc}
   */
  public function fieldType() {
    return $this->pluginDefinition['fieldType'];
  }

  /**
   * {@inheritdoc}
   */
  public function addRequiredTextToLabel(array &$row) {
    if ($this->fConfig->isRequired()) {
      $markup = '<div class="field-ui-extras-manage-fields-label">';
      $markup .= $this->fConfig->getLabel();
      $markup .= '<span class="required">' . $this->t('required') . '</span>';
      $markup .= '</div>';
      $row['data']['label'] = [
        'data' => [
          '#markup' => $markup,
        ],
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addDefaultValueToItems() {
    if (!empty($this->fConfig->get('default_value'))) {
      $default = $this->fConfig->get('default_value');

      if (!empty($default[0]['value'])) {
        $this->items['Default'] = [
          'mode' => 'inline',
          'value' => Unicode::truncate(strip_tags($default[0]['value']), 30,
            FALSE, TRUE),
        ];
      }

      if (!empty($default[0]['format'])) {
        $this->items['Default format'] = [
          'mode' => 'inline',
          'value' => $default[0]['format'],
        ];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addDescriptionToItems() {
    if (!empty($this->fConfig->getDescription())) {
      $this->items['Description'] = [
        'mode' => 'inline',
        'value' => Unicode::truncate($this->fConfig->getDescription(), 30,
          FALSE, TRUE),
      ];
    }
    else {
      $this->items['Description'] = [
        'mode' => 'inline',
        'value' => $this->t('<span class="warning">Not specified</span>'),
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function addCardinalityValueToItems() {
    $cardinality = $this->fStorageConf->getCardinality();
    if ($cardinality === -1) {
      $cardinality = $this->t('Unlimited');
    }

    $this->items['Limit'] = [
      'mode' => 'inline',
      'value' => $cardinality,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    // Plugins will use this method to add items to the array,
    // or they will do whatever they'd like to the output via processRow();
  }

  /**
   * {@inheritdoc}
   */
  public function processRow(array $row) {
    $machine_name_markup = '<span class="machine-name">' .
      $this->fConfig->getName() . '</span>';

    $info_markup = '<div class="info-wrapper">';
    $info_markup .= '<ul class="main">';

    foreach ($this->items as $label => $item) {
      $info_markup .= '<li>';
      $info_markup .= '<span class="label">' . $label . ': </span>';

      if ($item['mode'] == 'inline') {
        $info_markup .= $item['value'];
      }
      elseif ($item['mode'] == 'list') {
        $info_markup .= '<ul><li>';
        $info_markup .= implode('</li><li>', $item['items']);
        $info_markup .= '</li></ul>';
      }

      $info_markup .= '</li>';
    }

    $info_markup .= '</ul>';
    $info_markup .= '</div>';

    $row['data']['field_name'] = [
      'data' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['field-ui-extras-manage-fields-machine-name'],
        ],
        'label' => [
          '#markup' => $machine_name_markup,
        ],
        'details' => [
          '#markup' => $info_markup,
        ],
        '#attached' => [
          'library' => [
            'field_ui_extras/summary',
          ],
        ],
      ],
    ];

    return $row;
  }

}
