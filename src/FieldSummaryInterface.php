<?php

namespace Drupal\field_ui_extras;

/**
 * Defines a FieldSummaryInterface interface.
 *
 * This will ensure that consumers of the plugin have a consistent way of
 * accessing the plugin's functionality. This should include any public
 * properties and methods needed to accomplish whatever business logic anyone
 * accessing the plugin might want to use.
 *
 * @package Drupal\field_ui_extras
 */
interface FieldSummaryInterface {

  /**
   * Provides the type of field the plugin handles.
   *
   * @return string
   *   Type of field (e.g., text, date, datetime, file, image, decimal, etc.).
   */
  public function fieldType();

  /**
   * Appends "required" to field label if necessary.
   *
   * @param array $row
   *   Row of items for the table output.
   */
  public function addRequiredTextToLabel(array &$row);

  /**
   * Adds the standard default value information to the items array.
   */
  public function addDefaultValueToItems();

  /**
   * Adds the cardinality information to the items array.
   */
  public function addCardinalityValueToItems();

  /**
   * Adds the field description (help text) to the items array.
   */
  public function addDescriptionToItems();

  /**
   * Plugins will use this method to add items to the items array.
   *
   * The items array will be rendered by processRow() unless the plugin
   * overrides processRow to not leverage $this->items;
   */
  public function populateItemsArray();

  /**
   * Returns a render array for the field instance.
   *
   * FieldSummaryBase will, by default, render $this->items as an
   * unordered list. Plugins are free to render whatever they'd like (by
   * returning whatever they'd like in the render array).
   *
   * @param array $row
   *   Array of row data (may have already been altered by other plugins).
   *
   * @return array
   *   Render array for the field row.
   */
  public function processRow(array $row);

}
