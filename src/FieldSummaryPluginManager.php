<?php

namespace Drupal\field_ui_extras;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\field_ui_extras\Annotation\FieldSummary;

/**
 * A plugin manager for FieldSummary plugins.
 *
 * This plugin manager is also declared as a service in
 * field_ui_extras.services.yml so that it can be easily acces and used
 * anytime we need to work with FieldSummary plugins.
 *
 * @package Drupal\field_ui_extras
 */
class FieldSummaryPluginManager extends DefaultPluginManager {

  /**
   * Creates the discovery object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    // Tell the plugin manager to look for FieldSummary plugins here.
    // This also defines the PSR-4 subnamespace in which FieldSummary
    // plugins will live. Modules can put a plugin class in their own namespace
    // such as Drupal\{mymodule}\Plugin\FieldUiExtras\FieldSummary\MyFieldType.
    $subdir = 'Plugin/FieldUiExtras/FieldSummary';

    // Name of the interface that plugins should adhere to. Drupal will enforce
    // this as a requirement. Drupal will throw an error if a plugin does not
    // implement this interface.
    $plugin_interface = FieldSummaryInterface::class;

    // Name of the annotation class that contains the plugin definition.
    $plugin_definition_annotation_name = FieldSummary::class;

    parent::__construct($subdir, $namespaces, $module_handler, $plugin_interface, $plugin_definition_annotation_name);

    // Allow modules to alter plugin definitions using standard alter hook
    // implementation functions.
    // Use hook_field_summary_info_alter() for ours.
    // See field_ui_extras_field_summary_info_alter().
    $this->alterInfo('field_summary_info');

    // Set the caching method for our plugin definitions.
    // The annotations are read and then the resulting data is cached using
    // the provided cache backend. In field_ui_extras.services.yml we've
    // specified the @cache.default service to be used for our plugin.
    // The plugin definition will be stored in the cache_default table with
    // the specified cache prefix (second argument).
    $this->setCacheBackend($cache_backend, 'field_summary_info');
  }

}
