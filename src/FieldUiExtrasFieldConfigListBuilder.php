<?php

namespace Drupal\field_ui_extras;

use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\FieldTypePluginManagerInterface;
use Drupal\field_ui\FieldConfigListBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Overrides rows in Field UI's "Manage Fields" table.
 *
 * @package Drupal\StructureIndex
 */
class FieldUiExtrasFieldConfigListBuilder extends FieldConfigListBuilder {

  /**
   * FieldSummary plugin manager.
   *
   * @var \Drupal\field_ui_extras\FieldSummaryPluginManager
   */
  protected $fieldSummaryManager;

  /**
   * Stores all of the chosen (on settings form) plugins for each field type.
   *
   * @var array
   */
  protected $pluginsByFieldType = [];

  /**
   * FieldUiExtrasFieldConfigListBuilder constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   The entity type.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_manager
   *   The entity manager.
   * @param \Drupal\Core\Field\FieldTypePluginManagerInterface $field_type_manager
   *   The field type manager.
   * @param \Drupal\field_ui_extras\FieldSummaryPluginManager $field_summary_manager
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function __construct(EntityTypeInterface $entity_type, EntityTypeManagerInterface $entity_manager, FieldTypePluginManagerInterface $field_type_manager, FieldSummaryPluginManager $field_summary_manager, EntityFieldManagerInterface $entity_field_manager) {
    parent::__construct($entity_type, $entity_manager, $field_type_manager, $entity_field_manager);

    // Get all available plugins.
    $this->fieldSummaryManager = $field_summary_manager;
    $field_summary_definitions = $this->fieldSummaryManager->getDefinitions();

    // Get array of plugins actually needed (as configured via settings form).
    $config = \Drupal::config('field_ui_extras.settings');
    $mapped_plugins = $config->get('field_type_summary_plugin_mappings');
    $mapped_plugins = array_flip($mapped_plugins);

    // Create a single instance for each plugin that is actually needed.
    foreach ($field_summary_definitions as $plugin_id => $plugin_definition) {
      if (!empty($mapped_plugins[$plugin_id])) {
        $plugin = $this->fieldSummaryManager->createInstance($plugin_id, []);
        $this->pluginsByFieldType[$plugin_definition['fieldType']] = $plugin;
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('plugin.manager.field_ui_extras.field_summary'),
      $container->get('entity_field.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header = parent::buildHeader();
    $header['field_name'] = [
      'data' => $this->t('Configuration'),
      'class' => [
        RESPONSIVE_PRIORITY_MEDIUM,
      ],
    ];

    return $header;
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $field_config) {
    /** @var \Drupal\field\FieldConfigInterface $field_config */
    $field_type = $field_config->getType();
    $row = parent::buildRow($field_config);
    if (empty($this->pluginsByFieldType[$field_type])) {
      return $row;
    }

    $plugin = $this->pluginsByFieldType[$field_type];
    $plugin->fConfig = $field_config;
    $plugin->fStorageConf = $field_config->getFieldStorageDefinition();
    $plugin->fStorageSettings = $plugin->fStorageConf->getSettings();
    $plugin->fWidgetConf = $plugin->fConfig->getSettings();
    $plugin->items = [];

    $plugin->addCardinalityValueToItems();
    $plugin->addRequiredTextToLabel($row);
    $plugin->addDescriptionToItems();
    $plugin->addDefaultValueToItems();

    $plugin->populateItemsArray();
    $row = $plugin->processRow($row);

    return $row;
  }

}
