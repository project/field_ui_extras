<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

use Drupal\field_ui_extras\FieldSummaryBase;

/**
 * Provides field config list info for file fields.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_file",
 *   fieldType = "file",
 * )
 */
class File extends FieldSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    if (!empty($this->fWidgetConf['display_field'])) {
      $this->items['Display field'] = [
        'mode' => 'inline',
        'value' => $this->t('Enabled'),
      ];
    }

    if (!empty($this->fWidgetConf['display_default'])) {
      $this->items['Files displayed by default'] = [
        'mode' => 'inline',
        'value' => $this->t('True'),
      ];
    }

    if (!empty($this->fWidgetConf['file_directory'])) {
      $this->items['Dir'] = [
        'mode' => 'inline',
        'value' => $this->fWidgetConf['file_directory'],
      ];
    }
    else {
      $this->items['Dir'] = [
        'mode' => 'inline',
        'value' => $this->t('<span class="warning">Not specified</span>'),
      ];
    }

    if (!empty($this->fWidgetConf['uri_scheme'])) {
      $this->items['URI scheme'] = [
        'mode' => 'inline',
        'value' => $this->fWidgetConf['uri_scheme'],
      ];
    }

    if (!empty($this->fWidgetConf['file_extensions'])) {
      $this->items['Exts'] = [
        'mode' => 'inline',
        'value' => str_replace(' ', ', ',
          $this->fWidgetConf['file_extensions']),
      ];
    }
    else {
      $this->items['Exts'] = [
        'mode' => 'inline',
        'value' => $this->t('<span class="warning">Not specified</span>'),
      ];
    }

    if (!empty($this->fWidgetConf['max_filesize'])) {
      $this->items['Max size'] = [
        'mode' => 'inline',
        'value' => $this->fWidgetConf['max_filesize'],
      ];
    }
    else {
      $this->items['Max size'] = [
        'mode' => 'inline',
        'value' => $this->t('<span class="warning">No limit</span>'),
      ];
    }
  }

}
