<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

use Drupal\field_ui_extras\FieldSummaryBase;

/**
 * Provides field config list info for decimal fields.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_decimal",
 *   fieldType = "decimal",
 * )
 */
class Decimal extends FieldSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function addDefaultValueToItems() {
    $prefix = '';
    if (!empty($this->fWidgetConf['prefix'])) {
      $prefix = $this->fWidgetConf['prefix'];
    }

    $suffix = '';
    if (!empty($this->fWidgetConf['suffix'])) {
      $suffix = $this->fWidgetConf['suffix'];
    }

    if (!empty($this->fConfig->get('default_value'))) {
      $default = $this->fConfig->get('default_value');
      if (!empty($default[0]['value'])) {
        $this->items['Default'] = [
          'mode' => 'inline',
          'value' => $prefix . $default[0]['value'] . $suffix,
        ];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    if (!empty($this->fStorageSettings['precision'])) {
      $this->items['Precision (total digits)'] = [
        'mode' => 'inline',
        'value' => $this->fStorageSettings['precision'],
      ];
    }

    if (!empty($this->fStorageSettings['scale'])) {
      $this->items['Scale (digits to right of decimal)'] = [
        'mode' => 'inline',
        'value' => $this->fStorageSettings['scale'],
      ];
    }

    if (!empty($this->fWidgetConf['min'])) {
      $this->items['Minimum'] = [
        'mode' => 'inline',
        'value' => $this->fWidgetConf['min'],
      ];
    }

    if (!empty($this->fWidgetConf['max'])) {
      $this->items['Maximum'] = [
        'mode' => 'inline',
        'value' => $this->fWidgetConf['max'],
      ];
    }

    $prefix = '';
    if (!empty($this->fWidgetConf['prefix'])) {
      $prefix = $this->fWidgetConf['prefix'];
    }

    $suffix = '';
    if (!empty($this->fWidgetConf['suffix'])) {
      $suffix = $this->fWidgetConf['suffix'];
    }

    if (!empty($prefix)) {
      $this->items['Prefix'] = [
        'mode' => 'inline',
        'value' => $prefix,
      ];
    }

    if (!empty($suffix)) {
      $this->items['Suffix'] = [
        'mode' => 'inline',
        'value' => $suffix,
      ];
    }
  }

}
