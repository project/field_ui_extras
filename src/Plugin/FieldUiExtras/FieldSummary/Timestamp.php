<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

use Drupal\field_ui_extras\FieldSummaryBase;

/**
 * Provides field config list info for timestamp fields.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_timestamp",
 *   fieldType = "timestamp",
 * )
 */
class Timestamp extends FieldSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function addDefaultValueToItems() {
    if (!empty($this->fConfig->get('default_value'))) {
      $default = $this->fConfig->get('default_value');
      if (!empty($default[0]['value'])) {
        $this->items['Default'] = [
          'mode' => 'inline',
          'value' => \Drupal::service('date.formatter')
            ->format($default[0]['value'], 'long'),
        ];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    // See $this->addDefaultValueToItems() above.
  }

}
