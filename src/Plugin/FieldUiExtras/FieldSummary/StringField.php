<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

use Drupal\field_ui_extras\FieldSummaryBase;

/**
 * Provides field config list info for string (text (plain)) fields.
 *
 * "String" is a reserved word, so we're calling this StringField.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_string_field",
 *   fieldType = "string",
 * )
 */
class StringField extends FieldSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    // There's nothing to do. $this->addDefaultValueToItems() covers this type.
  }

}
