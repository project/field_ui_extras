<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

use Drupal\field_ui_extras\FieldSummaryBase;

/**
 * Provides field config list info for text_with_summary fields.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_text_with_summary",
 *   fieldType = "text_with_summary",
 * )
 */
class TextWithSummary extends FieldSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    if (!empty($this->fConfig->get('third_party_settings'))) {
      $settings = $this->fConfig->get('third_party_settings');
      if (!empty($settings['allowed_formats'])) {
        $formats = [];
        foreach ($settings['allowed_formats'] as $format) {
          if ($format != '0') {
            $formats[] = $format;
          }
        }

        if (empty($formats)) {
          $formats[] = $this->t('All');
        }

        $this->items['Allowed formats'] = [
          'mode' => 'list',
          'items' => $formats,
        ];
      }
    }

    if (!empty($this->fWidgetConf['display_summary'])) {
      $val = $this->t('Yes');
    }
    else {
      $val = $this->t('No');
    }

    $this->items['Display summary'] = [
      'mode' => 'inline',
      'value' => $val,
    ];
  }

}
