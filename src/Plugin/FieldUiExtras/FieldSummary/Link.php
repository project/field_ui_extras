<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

use Drupal\field_ui_extras\FieldSummaryBase;

/**
 * Provides field config list info for link fields.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_link",
 *   fieldType = "link",
 * )
 */
class Link extends FieldSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    if (!empty($this->fWidgetConf['link_type'])) {
      if ($this->fWidgetConf['link_type'] == 1) {
        $type = $this->t('Internal only');
      }
      elseif ($this->fWidgetConf['link_type'] == 16) {
        $type = $this->t('External only');
      }
      elseif ($this->fWidgetConf['link_type'] == 17) {
        $type = $this->t('Internal/External');
      }
      else {
        $type = $this->t('Unknown');
      }

      $this->items['Link type'] = [
        'mode' => 'inline',
        'value' => $type,
      ];
    }

    if (isset($this->fWidgetConf['title'])) {
      if ($this->fWidgetConf['title'] == 0) {
        $title = $this->t('Disabled');
      }
      elseif ($this->fWidgetConf['title'] == 1) {
        $title = $this->t('Optional');
      }
      elseif ($this->fWidgetConf['title'] == 2) {
        $title = $this->t('Required');
      }
      else {
        $title = $this->t('Unknown');
      }

      $this->items['Link title'] = [
        'mode' => 'inline',
        'value' => $title,
      ];
    }
  }

}
