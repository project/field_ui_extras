<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

/**
 * Provides field config list info for entity reference revisions fields.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_entity_reference_revisions",
 *   fieldType = "entity_reference_revisions",
 * )
 */
class EntityReferenceRevisions extends EntityReference {
  // Everything is already covered by EntityReference.
}
