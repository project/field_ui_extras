<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

use Drupal\field_ui_extras\FieldSummaryBase;

/**
 * Provides field config list info for boolean fields.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_boolean",
 *   fieldType = "boolean",
 * )
 */
class Boolean extends FieldSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    if (!empty($this->fWidgetConf['on_label'])) {
      $this->items['On label'] = [
        'mode' => 'inline',
        'value' => $this->fWidgetConf['on_label'],
      ];
    }

    if (!empty($this->fWidgetConf['off_label'])) {
      $this->items['Off label'] = [
        'mode' => 'inline',
        'value' => $this->fWidgetConf['off_label'],
      ];
    }
  }

}
