<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

use Drupal\field_ui_extras\FieldSummaryBase;

/**
 * Provides field config list info for text fields.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_text",
 *   fieldType = "text",
 * )
 */
class Text extends FieldSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    if (!empty($this->fConfig->get('third_party_settings'))) {
      $settings = $this->fConfig->get('third_party_settings');
      if (!empty($settings['allowed_formats'])) {
        $formats = [];
        foreach ($settings['allowed_formats'] as $format) {
          if ($format != '0') {
            $formats[] = $format;
          }
        }

        if (empty($formats)) {
          $formats[] = $this->t('All');
        }

        $this->items['Allowed formats'] = [
          'mode' => 'list',
          'items' => $formats,
        ];
      }
    }

    if (!empty($this->fStorageSettings['max_length'])) {
      $this->items['Max length'] = [
        'mode' => 'inline',
        'value' => $this->fStorageSettings['max_length'],
      ];
    }
  }

}
