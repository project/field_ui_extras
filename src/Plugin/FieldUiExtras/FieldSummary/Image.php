<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

use Drupal\field_ui_extras\FieldSummaryBase;

/**
 * Provides field config list info for image fields.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_image",
 *   fieldType = "image",
 * )
 */
class Image extends FieldSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    if (!empty($this->fWidgetConf['display_field'])) {
      $this->items['Display field'] = [
        'mode' => 'inline',
        'value' => $this->t('Enabled'),
      ];
    }

    if (!empty($this->fWidgetConf['display_default'])) {
      $this->items['Files displayed by default'] = [
        'mode' => 'inline',
        'value' => $this->t('True'),
      ];
    }

    if (!empty($this->fWidgetConf['file_directory'])) {
      $this->items['Dir'] = [
        'mode' => 'inline',
        'value' => $this->fWidgetConf['file_directory'],
      ];
    }
    else {
      $this->items['Dir'] = [
        'mode' => 'inline',
        'value' => $this->t('<span class="warning">Not specified</span>'),
      ];
    }

    if (!empty($this->fWidgetConf['uri_scheme'])) {
      $this->items['URI scheme'] = [
        'mode' => 'inline',
        'value' => $this->fWidgetConf['uri_scheme'],
      ];
    }

    if (!empty($this->fWidgetConf['file_extensions'])) {
      $this->items['Exts'] = [
        'mode' => 'inline',
        'value' => str_replace(' ', ', ',
          $this->fWidgetConf['file_extensions']),
      ];
    }
    else {
      $this->items['Exts'] = [
        'mode' => 'inline',
        'value' => $this->t('<span class="warning">Not specified</span>'),
      ];
    }

    if (!empty($this->fWidgetConf['max_filesize'])) {
      $this->items['Max size'] = [
        'mode' => 'inline',
        'value' => $this->fWidgetConf['max_filesize'],
      ];
    }
    else {
      $this->items['Max size'] = [
        'mode' => 'inline',
        'value' => $this->t('<span class="warning">No limit</span>'),
      ];
    }

    if (!empty($this->fWidgetConf['max_resolution'])) {
      $this->items['Max resolution'] = [
        'mode' => 'inline',
        'value' => $this->fWidgetConf['max_resolution'],
      ];
    }

    if (!empty($this->fWidgetConf['min_resolution'])) {
      $this->items['Min resolution'] = [
        'mode' => 'inline',
        'value' => $this->fWidgetConf['min_resolution'],
      ];
    }

    $alt = '';
    if (!empty($this->fWidgetConf['alt_field'])) {
      $alt .= $this->t('Enabled');
    }
    else {
      $alt .= $this->t('<span class="warning">Disabled</span>');
    }
    $alt .= ' & ';
    if (!empty($this->fWidgetConf['alt_field_required'])) {
      $alt .= $this->t('Required');
    }
    else {
      $alt .= $this->t('<span class="warning">Not required</span>');
    }

    $this->items['Alt text'] = [
      'mode' => 'inline',
      'value' => $alt,
    ];

    $title = '';
    if (!empty($this->fWidgetConf['title_field'])) {
      $title .= $this->t('Enabled');
    }
    else {
      $title .= $this->t('Disabled');
    }
    $title .= ' & ';
    if (!empty($this->fWidgetConf['title_field_required'])) {
      $title .= $this->t('Required');
    }
    else {
      $title .= $this->t('Not required');
    }

    $this->items['Title text'] = [
      'mode' => 'inline',
      'value' => $title,
    ];

    if (!empty($this->fWidgetConf['default_image']['uuid'])) {
      $this->items['Has default image'] = [
        'mode' => 'inline',
        'value' => $this->t('True'),
      ];
    }

    if (!empty($this->fWidgetConf['default_image']['alt'])) {
      $this->items['Default alt text'] = [
        'mode' => 'inline',
        'value' => $this->fWidgetConf['default_image']['alt'],
      ];
    }

    if (!empty($this->fWidgetConf['default_image']['title'])) {
      $this->items['Default title text'] = [
        'mode' => 'inline',
        'value' => $this->fWidgetConf['default_image']['title'],
      ];
    }
  }

}
