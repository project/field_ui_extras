<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\field_ui_extras\FieldSummaryBase;

/**
 * Provides field config list info for viewsreference fields.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_viewsreference",
 *   fieldType = "viewsreference",
 * )
 */
class ViewsReference extends FieldSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    if (!empty($this->fWidgetConf['handler_settings']['auto_create'])) {
      $this->items['Auto create'] = [
        'mode' => 'inline',
        'value' => $this->t('True'),
      ];
    }

    if (!empty($this->fWidgetConf['plugin_types'])) {
      $plugin_types = [];
      foreach ($this->fWidgetConf['plugin_types'] as $type) {
        if (!empty($type)) {
          $plugin_types[] = $type;
        }
      }

      if (!empty($plugin_types)) {
        $this->items['Display plugins'] = [
          'mode' => 'list',
          'items' => $plugin_types,
        ];
      }
    }

    if (!empty($this->fWidgetConf['preselect_views'])) {
      $view_names = [];
      foreach ($this->fWidgetConf['preselect_views'] as $view_name) {
        if (!empty($view_name)) {
          $view_names[] = Link::fromTextAndUrl($view_name,
            Url::fromUserInput('/admin/structure/views/view/' . $view_name))
            ->toString();
        }
      }

      if (!empty($view_names)) {
        $this->items['Preselected views'] = [
          'mode' => 'list',
          'items' => $view_names,
        ];
      }
    }

    if (!empty($this->fWidgetConf['enabled_settings'])) {
      $settings = [];
      foreach ($this->fWidgetConf['enabled_settings'] as $setting) {
        if (!empty($setting)) {
          $settings[] = $setting;
        }
      }

      if (!empty($settings)) {
        $this->items['Enabled settings'] = [
          'mode' => 'list',
          'items' => $settings,
        ];
      }
    }
  }

}
