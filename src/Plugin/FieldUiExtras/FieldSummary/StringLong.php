<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

use Drupal\field_ui_extras\FieldSummaryBase;

/**
 * Provides field config list info for string (text (plain, long)) fields.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_string_long",
 *   fieldType = "string_long",
 * )
 */
class StringLong extends FieldSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    // There's nothing to do. $this->addDefaultValueToItems() covers this type.
  }

}
