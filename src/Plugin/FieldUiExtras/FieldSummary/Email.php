<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

use Drupal\field_ui_extras\FieldSummaryBase;

/**
 * Provides field config list info for email fields.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_email",
 *   fieldType = "email",
 * )
 */
class Email extends FieldSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    // There's nothing to do. $this->addDefaultValueToItems() covers this type.
  }

}
