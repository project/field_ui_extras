<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

use Drupal\field_ui_extras\FieldSummaryBase;

/**
 * Provides field config list info for text_long fields.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_text_long",
 *   fieldType = "text_long",
 * )
 */
class TextLong extends FieldSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    if (!empty($this->fConfig->get('third_party_settings'))) {
      $settings = $this->fConfig->get('third_party_settings');
      if (!empty($settings['allowed_formats'])) {
        $formats = [];
        foreach ($settings['allowed_formats'] as $format) {
          if ($format != '0') {
            $formats[] = $format;
          }
        }

        if (empty($formats)) {
          $formats[] = $this->t('All');
        }

        $this->items['Allowed formats'] = [
          'mode' => 'list',
          'items' => $formats,
        ];
      }
    }
  }

}
