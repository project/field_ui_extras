<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\field_ui_extras\FieldSummaryBase;

/**
 * Provides field config list info for entity reference fields.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_entity_reference",
 *   fieldType = "entity_reference",
 * )
 */
class EntityReference extends FieldSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    $entity_type_mgr = \Drupal::entityTypeManager();
    $entity_type = $this->fStorageSettings['target_type'];
    $this->items['Ref type'] = [
      'mode' => 'inline',
      'value' => $entity_type_mgr->getDefinition($entity_type)->getLabel(),
    ];

    $this->addBundleLinksToItems();
    $this->addHandlerSettingsToItems();
  }

  /**
   * Adds bundle links to the items array.
   */
  public function addBundleLinksToItems() {
    $entity_type_mgr = \Drupal::entityTypeManager();
    $entity_type = $this->fStorageSettings['target_type'];

    $bundle_entity_type = $entity_type_mgr
      ->getDefinition($entity_type)
      ->getBundleEntityType();

    $label = $this->t('@type bundles', [
      '@type' => $entity_type_mgr->getDefinition($entity_type)->getLabel(),
    ]);

    $all_lines = [];

    $bundles = $this->fWidgetConf['handler_settings']['target_bundles'];

    if (!empty($this->fWidgetConf['handler_settings']['negate'])
      && !empty($this->fWidgetConf['handler_settings']['target_bundles_drag_drop'])) {
      $bundles = $this->getNegatedUncheckedBundles(
        $this->fWidgetConf['handler_settings']['target_bundles_drag_drop']);
    }

    if (!empty($bundles)) {
      foreach ($bundles as $bundle) {
        $all_lines[] = $this->addBundleLinksToItemsProcessBundle(
          $entity_type_mgr, $bundle, $bundle_entity_type, $entity_type);
      }
    }

    if (!empty($all_lines)) {
      $this->items[(string) $label] = [
        'mode' => 'list',
        'items' => $all_lines,
      ];
    }
  }

  /**
   * Returns an array of unchecked bundles from an "Exclude" mode ref type.
   *
   * When using "Exclude the selected below" the checked items are "enabled"
   * but this really means we will ignore them and not the others. So, this
   * method will return the disabled (aka to-be-included) bundles.
   *
   * This may only exist for the paragraph reference type.
   *
   * @param array $bundles
   *   Array of all bundles.
   *
   * @return array
   *   Array of target bundle machine names.
   */
  public function getNegatedUncheckedBundles(array $bundles) {
    $target_bundles = [];

    foreach ($bundles as $bundle => $settings) {
      if ($settings['enabled'] === FALSE) {
        $target_bundles[] = $bundle;
      }
    }

    return $target_bundles;
  }

  /**
   * Returns HTML for the bundle line: Bundle Label (fields form display).
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_mgr
   *   Entity type manager.
   * @param $bundle
   *   Bundle machine name.
   * @param string $bundle_entity_type
   *   Bundle entity type (node_type, taxonomy_vocabulary, etc).
   * @param $entity_type
   *   Entity type (node, taxonomy_term, etc).
   *
   * @return string
   *   HTML for the bundle line (all links): Bundle Label (fields form display).
   */
  public function addBundleLinksToItemsProcessBundle(EntityTypeManagerInterface $entity_type_mgr, $bundle, $bundle_entity_type, $entity_type) {
    $params = [$bundle_entity_type => $bundle];

    if (!empty($bundle_entity_type)) {
      $label = $entity_type_mgr
        ->getStorage($bundle_entity_type)->load($bundle)->label();
    }
    else {
      // Known bundles without bundle_entity_type values:
      // Custom menu link.
      $label = (string) $entity_type_mgr->getDefinition($entity_type)
        ->getLabel();
    }

    try {
      $edit_link = Link::createFromRoute($label, 'entity.' .
        $bundle_entity_type . '.edit_form', $params);
      $bundle_line = $edit_link->toString();
    }
    catch (\Exception $e) {
      $bundle_line = $label;
    }

    $links = [];
    try {
      $fields_link = Link::createFromRoute('fields', 'entity.' .
        $entity_type . '.field_ui_fields', $params);
      $links[] = $fields_link->toString();
    }
    catch (\Exception $e) {
      // Do nothing.
    }

    try {
      $form_link = Link::createFromRoute('form', 'entity.entity_form_display.' .
        $entity_type . '.default', $params);
      $links[] = $form_link->toString();
    }
    catch (\Exception $e) {
      // Do nothing.
    }

    try {
      $display_link = Link::createFromRoute('display', 'entity.entity_view_display.' .
        $entity_type . '.default', $params);
      $links[] = $display_link->toString();
    }
    catch (\Exception $e) {
      // Do nothing.
    }

    if (!empty($links)) {
      $bundle_line .= ' (';
      $bundle_line .= implode(' &middot; ', $links);
      $bundle_line .= ')';
    }

    return $bundle_line;
  }

  /**
   * Adds handler settings to the items array.
   */
  public function addHandlerSettingsToItems(): void {
    $handler = $this->fWidgetConf['handler_settings'];
    if (!empty($handler['sort']['field'])
      && $handler['sort']['field'] != '_none') {
      $this->items['Sort field'] = [
        'mode' => 'inline',
        'value' => $handler['sort']['field'],
      ];
    }

    if (!empty($handler['sort']['direction'])) {
      $this->items['Sort direction'] = [
        'mode' => 'inline',
        'value' => strtolower($handler['sort']['direction']),
      ];
    }

    if (!empty($handler['auto_create'])) {
      $this->items['Auto create'] = [
        'mode' => 'inline',
        'value' => $this->t('True'),
      ];
    }

    if (!empty($handler['auto_create_bundle'])) {
      $this->items['Auto create bundle'] = [
        'mode' => 'inline',
        'value' => $handler['auto_create_bundle'],
      ];
    }

    if (!empty($handler['include_anonymous'])) {
      $this->items['Include anonymous'] = [
        'mode' => 'inline',
        'value' => $this->t('True'),
      ];
    }

    if (!empty($handler['filter']['type'])) {
      // TODO This is user specific; consider moving into separate plugin.
      if ($handler['filter']['type'] == 'role'
        && !empty($handler['filter']['role'])) {
        $this->items['Roles'] = [
          'mode' => 'inline',
          'value' => implode(', ', $handler['filter']['role']),
        ];
      }
    }
  }

}
