<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

use Drupal\field_ui_extras\FieldSummaryBase;

/**
 * Provides field config list info for list_integer fields.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_list_integer",
 *   fieldType = "list_integer",
 * )
 */
class ListInteger extends FieldSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    if (!empty($this->fWidgetConf['allowed_values'])) {
      $allowed_vals_arr = $this->fWidgetConf['allowed_values'];

      if (count($allowed_vals_arr) > 10) {
        $too_many = TRUE;
        $allowed_vals_arr = array_slice($allowed_vals_arr, 0, 5, TRUE);
      }

      $vals = [];
      foreach ($allowed_vals_arr as $key => $value) {
        $vals[] = $key . '|' . $value;
      }

      if (!empty($too_many)) {
        $vals[] = $this->t('<em>... only showing first 5</em>');
      }

      $this->items['Allowed values'] = [
        'mode' => 'list',
        'items' => $vals,
      ];
    }
  }

}
