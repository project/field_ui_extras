<?php

namespace Drupal\field_ui_extras\Plugin\FieldUiExtras\FieldSummary;

use Drupal\field_ui_extras\FieldSummaryBase;

/**
 * Provides field config list info for datetime fields.
 *
 * @FieldSummary(
 *   id = "field_ui_extras_datetime",
 *   fieldType = "datetime",
 * )
 */
class DateTime extends FieldSummaryBase {

  /**
   * {@inheritdoc}
   */
  public function addDefaultValueToItems() {
    if (!empty($this->fConfig->get('default_value'))) {
      $default = $this->fConfig->get('default_value');

      if (!empty($default[0]['default_date_type'])) {
        $this->items['Default type'] = [
          'mode' => 'inline',
          'value' => ucfirst($default[0]['default_date_type']),
        ];
      }

      if (!empty($default[0]['default_date'])) {
        $this->items['Default value'] = [
          'mode' => 'inline',
          'value' => $default[0]['default_date'],
        ];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function populateItemsArray() {
    if (!empty($this->fStorageSettings['datetime_type'])) {
      switch ($this->fStorageSettings['datetime_type']) {
        case 'date':
          $type = $this->t('Date only');
          break;

        case 'datetime':
          $type = $this->t('Date and time');
          break;

        default:
          $type = $this->t('Unknown');
          break;
      }

      $this->items['Date type'] = [
        'mode' => 'inline',
        'value' => $type,
      ];
    }
  }

}
