CONTENTS OF THIS FILE
---------------------

  * Introduction
  * Requirements
  * Installation
  * Configuration
  * Maintainers


INTRODUCTION
------------

  This module provides additional information on Field UI screens
  (e.g., Manage Fields). The main goal of the project is to reduce the number
  of clicks it takes to get information about the fields on your site.
  
  Extras Include:
  
  * "Required" indicator
  * Core field widget settings
  * Core field settings
  * Additional warnings and information for your convenience:
    * Entity Reference: links to chosen bundle fields, form, and display
    * File/Image: warning if alt text not required, dir or max size unspecified
  
  Future Extras May Include:
  
  * Field info will include which displays each field appears within


REQUIREMENTS
------------

  * Field
  * Field UI


INSTALLATION
------------

  * Install as you would normally install a contributed Drupal module. See:
    https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
    for further information.
  * Field UI Extras provides summary plugins for all of the core field types.
    After installing you will see field summaries on Manage Fields screens.


CONFIGURATION
-------------

  You can configuration which field summary plugins are mapped to which field
  types via the form at admin/config/system/field_ui_extras.


EXTENDING
---------
  Define your own FieldSummary plugins by copying one of the existing plugins
  from field_ui_extras/src/Plugin/FieldUiExtras/FieldSummary/ and having a look
  through \Drupal\field_ui_extras\FieldSummaryBase.

  After you define a plugin you can map this plugin to a field type via the
  configuration form documented above.


MAINTAINERS
-----------

  Current maintainers:
    * Adam Courtemanche - agileadam - http://www.agileadam.com
